import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPostandUsers } from '../actions';
import UserHeader from './UserHeader';

class PostList extends Component {
  componentDidMount() {
    this.props.fetchPostandUsers();
  }

  render() {
    return (
      <div>
        <h1>PostList</h1>
        <div className="row">
          {this.props.posts && this.renderList()}
        </div>
      </div>
    );
  }

  renderList() {
    return (
      this.props.posts.map(post =>
        <div className="col-sm-4 py-2" key={post.id}>
          <div className="card h-100 border-primary">
            <div className="card-header text-truncate text-uppercase bg-primary text-white">{post.title}</div>
            <div className="card-body">
              <UserHeader userId={post.userId} />
              <p className="card-text">{post.body}</p>
              <button className="btn btn-outline-primary">See post</button>
            </div>
          </div>
        </div>

      )
    );
  }
}

const mapStateToProps = (state) => {
  return { posts: state.posts };
}

export default connect(mapStateToProps, { fetchPostandUsers })(PostList);