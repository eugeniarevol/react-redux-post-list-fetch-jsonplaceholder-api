import React, { Component } from 'react';
import { connect } from 'react-redux';

class UserHeader extends Component {
  render() {
    const {user} = this.props;
    if(!user){
      return null;
    }

    return (
      <h6 className="card-title mb-2 text-muted">
        <i className="fas fa-user left mr-3 mb-3"></i>{user.name}
      </h6>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { user: state.users.find(user => user.id === ownProps.userId) };
}

export default connect(mapStateToProps)(UserHeader);