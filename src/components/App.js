import React, { Component, Fragment } from 'react';
import Navbar from './Navbar';
import PostList from './PostList';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <div className="container">
          <div className="row mt-5">
            <div className="col-sm"><PostList /></div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
