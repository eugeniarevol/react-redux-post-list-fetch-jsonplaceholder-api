import _ from 'lodash';
import jsonPlaceholder from '../apis/jsonPlaceholder';

export const fetchPostandUsers = () => async (dispatch, getState) => {
  await dispatch(fetchPosts());

  _.chain(getState().posts)
    .map('userId')
    .uniq()
    .forEach(id => dispatch(fetchUser(id)))
    .value();
}

// Action creator
export const fetchPosts = () => async dispatch => {
  // return a function thanks to react-thunk
  const response = await jsonPlaceholder.get('/posts')
  //since it's an async process 
  //we need to manually dispatch the action
  dispatch({
    type: 'FETCH_POSTS',
    payload: response.data
  })
}

export const fetchUser = (id) => async dispatch => {
  const response = await jsonPlaceholder.get(`/users/${id}`)
  dispatch({
    type: 'FETCH_USER',
    payload: response.data
  })
}